import jwt from 'jsonwebtoken';
import * as Yup from 'yup';
import crypto from 'crypto';

import authConfig from '../../config/auth';

import User from '../models/User';
import Portfolio from '../models/Portfolio';
import Enterprise from '../models/Enterprise';

class AuthController {
  async store(req, res) {
    const schema = Yup.object().shape({
      email: Yup.string()
        .email()
        .required(),
      password: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ errror: 'Validation fails' });
    }
    const { email, password } = req.body;

    const user = await User.findOne({
      where: { email },
    });

    if (!user) {
      return res.status(401).json({ error: 'User not found' });
    }

    if (!(await user.checkPassword(password))) {
      return res.status(401).json({ error: 'Password does not match' });
    }

    const {
      id,
      name,
      city,
      country,
      balance,
      photo,
      first_access,
      super_angel,
      enterprise,
    } = user;

    const client = crypto.randomBytes(16);
    await user.update({ client: client.toString('hex') });

    res.set({
      access_token: jwt.sign({ id }, authConfig.secret, {
        expiresIn: authConfig.expiresIn,
      }),
      uid: email,
      client: client.toString('hex'),
    });

    const portfolio = await Portfolio.findAll({
      where: {
        user_id: id,
      },
      include: [
        {
          model: Enterprise,
          as: 'enterprise',
          attributes: ['id', 'name'],
        },
      ],
    });

    const enterprisePortfolio = [];

    let portfolio_value = 0;

    portfolio.forEach(port => {
      if (enterprisePortfolio.indexOf(port.enterprise.name))
        enterprisePortfolio.push(port.enterprise.name);
      portfolio_value += port.shares * port.share_price;
    });
    let response = {};
    if (enterprise) {
      response = {
        investor: null,
        enterprise: {
          id,
          enterprise_name: name,
          city,
          country,
          balance,
          photo,
          first_access,
          super_angel,
        },
        success: true,
      };
    } else {
      response = {
        investor: {
          id,
          investor_name: name,
          city,
          country,
          balance,
          photo,
          portfolio: {
            enterprises_number: enterprisePortfolio.length,
            enterprises: enterprisePortfolio,
          },
          portfolio_value,
          first_access,
          super_angel,
        },
        enterprise: null,
        success: true,
      };
    }

    return res.json(response);
  }
}

export default new AuthController();
