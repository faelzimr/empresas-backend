import * as Yup from 'yup';
import { Op } from 'sequelize';
import Enterprise from '../models/Enterprise';
import EnterpriseType from '../models/EnterpriseType';
import User from '../models/User';

class EnterpriseController {
  async index(req, res) {
    const where = {};

    if (req.query.name) {
      const { name } = req.query;

      where.name = {
        [Op.iLike]: `%${name}%`,
      };
    }

    if (req.query.enterprise_types) {
      const { enterprise_types } = req.query;

      where.enterprise_type_id = enterprise_types;
    }

    const enterprise = await Enterprise.findAll({
      where,
      include: [
        {
          model: EnterpriseType,
          as: 'enterprise_type',
          attributes: ['id', ['name', 'enterprise_type_name']],
        },
      ],
      attributes: [
        'id',
        ['email', 'email_enterprise'],
        'facebook',
        'twitter',
        'linkedin',
        'phone',
        'own_enterprise',
        ['name', 'enterprise_name'],
        'photo',
        'description',
        'city',
        'country',
        'value',
        'share_price',
      ],
    });
    return res.json(enterprise);
  }

  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      email: Yup.string().email(),
      description: Yup.string().required(),
      city: Yup.string().required(),
      country: Yup.string().required(),
      facebook: Yup.string(),
      twitter: Yup.string(),
      linkedin: Yup.string(),
      phone: Yup.string(),
      own_enterprise: Yup.boolean(),
      enterprise_type_id: Yup.number().required(),
      value: Yup.number()
        .round('round')
        .required(),
      shares: Yup.number().required(),
      share_price: Yup.number()
        .round('round')
        .required(),
      own_shares: Yup.number().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validations fails' });
    }

    const { enterprise_type_id } = req.body;

    /**
     * Check if exist enterprise_type_id
     */
    const existsEnterpriseType = await EnterpriseType.findOne({
      where: { id: enterprise_type_id },
    });

    if (!existsEnterpriseType) {
      return res.status(401).json({ error: 'Enterprise type no exist' });
    }

    /**
     * Check if exist user_id
     */
    const existsUserEnterprise = await User.findOne({
      where: { id: req.userId, enterprise: true },
    });

    if (!existsUserEnterprise) {
      return res
        .status(401)
        .json({ error: 'Your user is not a enterprise type' });
    }

    const enterprise = await Enterprise.create({
      ...req.body,
      user_id: req.userId,
    });

    return res.json(enterprise);
  }
}

export default new EnterpriseController();
