import Enterprise from '../models/Enterprise';

class FileEnterpriseController {
  async store(req, res) {
    const { filename: path } = req.file;

    const { id } = req.params;

    const enterprise = await Enterprise.findByPk(id);

    if (!enterprise) {
      return res.status(404).json({ status: '404', error: 'Not Found' });
    }

    await enterprise.update({
      photo: `tmp/uploads/${path}`,
    });
    return res.json(enterprise);
  }
}

export default new FileEnterpriseController();
