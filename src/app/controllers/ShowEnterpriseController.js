import Enterprise from '../models/Enterprise';
import EnterpriseType from '../models/EnterpriseType';

class ShowEnterpriseController {
  async index(req, res) {
    const { id } = req.params;

    const enterprise = await Enterprise.findOne({
      where: { id },
      include: [
        {
          model: EnterpriseType,
          as: 'enterprise_type',
          attributes: ['id', ['name', 'enterprise_type_name']],
        },
      ],
      attributes: [
        'id',
        ['name', 'enterprise_name'],
        'description',
        ['email', 'email_enterprise'],
        'facebook',
        'twitter',
        'linkedin',
        'phone',
        'own_enterprise',
        'photo',
        'value',
        'shares',
        'share_price',
        'own_shares',
        'city',
        'country',
      ],
    });

    if (!enterprise) {
      return res.status(404).json({ status: '404', error: 'Not Found' });
    }
    const response = {
      enterprise,
      success: true,
    };
    return res.json(response);
  }
}

export default new ShowEnterpriseController();
