import * as Yup from 'yup';
import Portfolio from '../models/Portfolio';
import Enterprise from '../models/Enterprise';
import User from '../models/User';

class PortfolioController {
  async store(req, res) {
    const schema = Yup.object().shape({
      enterprise_id: Yup.number().required(),
      shares: Yup.number().required(),
      share_price: Yup.number()
        .round('round')
        .required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validations fails' });
    }

    const { enterprise_id } = req.body;

    /**
     * Check if exist enterprise_id
     */
    const existsEnterprise = await Enterprise.findOne({
      where: { id: enterprise_id },
    });

    if (!existsEnterprise) {
      return res.status(401).json({ error: 'Enterprise no exist' });
    }

    /**
     * Check if exist user_id
     */
    const existsUserInvestor = await User.findOne({
      where: { id: req.userId, enterprise: false },
    });

    if (!existsUserInvestor) {
      return res
        .status(401)
        .json({ error: 'Your user is not a investor type' });
    }

    const portfolio = await Portfolio.create({
      ...req.body,
      user_id: req.userId,
    });

    return res.json(portfolio);
  }
}

export default new PortfolioController();
