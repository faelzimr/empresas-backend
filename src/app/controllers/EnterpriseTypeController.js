import * as Yup from 'yup';
import EnterpriseType from '../models/EnterpriseType';

class EnterpriseTypeController {
  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ errror: 'Validation fails' });
    }

    const type = await EnterpriseType.create(req.body);

    return res.json(type);
  }
}

export default new EnterpriseTypeController();
