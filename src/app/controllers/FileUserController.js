import User from '../models/User';

class FileUserController {
  async store(req, res) {
    const { filename: path } = req.file;

    const { id } = req.params;

    const user = await User.findByPk(id);

    if (!user) {
      return res.status(404).json({ status: '404', error: 'Not Found' });
    }

    await user.update({
      photo: `tmp/uploads/${path}`,
    });
    return res.json(user);
  }
}

export default new FileUserController();
