import Sequelize, { Model } from 'sequelize';
import bcrypt from 'bcryptjs';

class User extends Model {
  static init(sequelize) {
    super.init(
      {
        name: Sequelize.STRING,
        email: Sequelize.STRING,
        password: Sequelize.VIRTUAL,
        password_hash: Sequelize.STRING,
        city: Sequelize.STRING,
        country: Sequelize.STRING,
        balance: Sequelize.DECIMAL,
        enterprise: Sequelize.BOOLEAN,
        first_access: Sequelize.BOOLEAN,
        super_angel: Sequelize.BOOLEAN,
        photo: Sequelize.STRING,
        client: Sequelize.STRING,
      },
      {
        sequelize,
      }
    );

    this.addHook('beforeSave', async user => {
      if (user.password) {
        user.password_hash = await bcrypt.hash(user.password, 8);
      }
    });

    return this;
  }

  checkPassword(password) {
    return bcrypt.compare(password, this.password_hash);
  }

  static associate(models) {
    this.hasMany(models.Portfolio, { foreignKey: 'user_id', as: 'portfolio' });
  }
}

export default User;
