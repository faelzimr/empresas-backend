import Sequelize, { Model } from 'sequelize';

class Portfolio extends Model {
  static init(sequelize) {
    super.init(
      {
        shares: Sequelize.INTEGER,
        share_price: Sequelize.DOUBLE,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.User, { foreignKey: 'user_id', as: 'user' });
    this.belongsTo(models.Enterprise, {
      foreignKey: 'enterprise_id',
      as: 'enterprise',
    });
  }
}

export default Portfolio;
