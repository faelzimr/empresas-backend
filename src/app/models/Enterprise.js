import Sequelize, { Model } from 'sequelize';

class Enterprise extends Model {
  static init(sequelize) {
    super.init(
      {
        name: Sequelize.STRING,
        email: Sequelize.STRING,
        facebook: Sequelize.STRING,
        twitter: Sequelize.STRING,
        linkedin: Sequelize.STRING,
        phone: Sequelize.STRING,
        own_enterprise: Sequelize.BOOLEAN,
        description: Sequelize.STRING,
        city: Sequelize.STRING,
        country: Sequelize.STRING,
        value: Sequelize.DOUBLE,
        shares: Sequelize.INTEGER,
        share_price: Sequelize.DOUBLE,
        own_shares: Sequelize.INTEGER,
        photo: Sequelize.STRING,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.User, { foreignKey: 'user_id', as: 'user' });
    this.belongsTo(models.EnterpriseType, {
      foreignKey: 'enterprise_type_id',
      as: 'enterprise_type',
    });
  }
}

export default Enterprise;
