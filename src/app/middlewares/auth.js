import jwt from 'jsonwebtoken';
import { promisify } from 'util';

import authConfig from '../../config/auth';

import User from '../models/User';

export default async (req, res, next) => {
  const { access_token, uid, client } = req.headers;

  if (!access_token) {
    return res.status(401).json({ error: 'Token not provided' });
  }
  try {
    const decoded = await promisify(jwt.verify)(
      access_token,
      authConfig.secret
    );

    req.userId = decoded.id;

    const checkUid = await User.findOne({
      where: { id: decoded.id, email: uid },
    });

    if (!checkUid) {
      return res.status(401).json({ error: 'Uid invalid' });
    }

    const checkClient = await User.findOne({
      where: { id: decoded.id, client },
    });

    if (!checkClient) {
      return res.status(401).json({ error: 'Client invalid' });
    }
    if (checkClient.first_access) {
      await checkClient.update({ first_access: false });
    }

    return next();
  } catch (err) {
    return res.status(401).json({ error: 'Token invalid' });
  }
};
