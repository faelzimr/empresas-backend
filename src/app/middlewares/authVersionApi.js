export default async (req, res, next) => {
  const { id } = req.params;
  if (id !== 'v1') {
    return res.status(400).json({ error: 'API version does not exist' });
  }
  return next();
};
