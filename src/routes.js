import { Router } from 'express';
import multer from 'multer';
import multerConfig from './config/multer';

import UserController from './app/controllers/UserController';
import AuthController from './app/controllers/AuthController';
import FileUserController from './app/controllers/FileUserController';
import EnterpriseTypeController from './app/controllers/EnterpriseTypeController';
import EnterpriseController from './app/controllers/EnterpriseController';
import FileEnterpriseController from './app/controllers/FileEnterpriseController';
import ShowEnterpriseController from './app/controllers/ShowEnterpriseController';
import PortfolioController from './app/controllers/PortfolioController';

import authUser from './app/middlewares/auth';
import authVersionApi from './app/middlewares/authVersionApi';

const routes = new Router();
const upload = multer(multerConfig);

routes.use('/api/v1', routes);
routes.use('/api/:id', authVersionApi);

routes.post('/users', UserController.store);
routes.post('/users/auth/sign_in', AuthController.store);

routes.use(authUser);

routes.put('/users', UserController.update);
routes.post(
  '/users/:id/files',
  upload.single('file'),
  FileUserController.store
);

routes.post('/enterprise_types', EnterpriseTypeController.store);

routes.post('/enterprises', EnterpriseController.store);
routes.get('/enterprises', EnterpriseController.index);
routes.get('/enterprises/:id', ShowEnterpriseController.index);
routes.post(
  '/enterprises/:id/files',
  upload.single('file'),
  FileEnterpriseController.store
);

routes.post('/portfolios', PortfolioController.store);

export default routes;
